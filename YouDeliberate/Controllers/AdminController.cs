﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq.Expressions;
using System.Web;
using System.Web.Mvc;
using YouDeliberate.Models.DAO;
using YouDeliberate.Models.Entity;
using YouDeliberate.Models.Services;
using System.Security.Cryptography;
using SystemWrapper.IO;

namespace YouDeliberate.Controllers
{
    public class AdminController : Controller
    {
        private ServiceRegistry registry;

        public AdminController()
        {
            registry = new ServiceRegistry();
        }

        public AdminController(IUploadService UploadService, IFileService FileService, 
                               INarrativeService NarrativeService, IAuthService AuthService)
        {
            registry = new ServiceRegistry();
            registry.UploadService = UploadService;
            registry.FileService = FileService;
            registry.NarrativeService = NarrativeService;
            registry.AuthService = AuthService;
        }

        public ActionResult Index()
        {
            IAuthService authService = registry.AuthService;
            return View(authService.VerifyLogin("Upload", Session));
        }

        public ActionResult Comments()
        {
            IAuthService authService = registry.AuthService;
            return View(authService.VerifyLogin("Comments", Session));
        }

        public ActionResult Config()
        {
            IAuthService authService = registry.AuthService;
            if (authService.VerifyLogin("Config", Session) != "Config")
            {
                return View("Index");
            }

            IAdminService adminService = registry.AdminService;
            List<administrator> adminList = adminService.GetAllAdmins();
            return View(adminList);
        }

        [HttpGet]
        public ActionResult AddAdmin(string user, string pass, string email)
        {
            administrator admin = new administrator();
            AdminService adminService = new AdminService();
            admin.username = user;
            admin.password = pass;
            admin.email = email;
            adminService.selectOption(admin,"add");
            return Json(true, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult ModAdmin(string user, string pass, string email)
        {
            administrator admin = new administrator();
            AdminService adminService = new AdminService();
            admin.username = user;
            admin.password = pass;
            admin.email = email;
            adminService.selectOption(admin,"mod");
            return Json(true, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult DelAdmin(int id)
        {
            AdminService adminService = new AdminService();
            adminService.delId = id;
            adminService.selectOption(null,"del");
            return Json(true, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult GetComments(int start, int end)
        {
            List<string> result = new List<string>();
            INarrativeService narrativeService = registry.NarrativeService;
            result = narrativeService.GetComments(start,end);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult DelComment(int id)
        {
            INarrativeService narrativeService = registry.NarrativeService;
            return Json(narrativeService.DelComment(id), JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult GetNrComments()
        {
            int result = 0;
            INarrativeService narrativeService = registry.NarrativeService;
            result = narrativeService.GetNrComments();
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult NewTopic(FormCollection form, HttpPostedFileBase uploadFile)
        {
            ITopicService topicService = registry.TopicService;
            topic topic = new topic();
            topic.content = form["topic"];
            topic.subcontent = form["subtopic"];

            HttpFileCollection UploadedFile = System.Web.HttpContext.Current.Request.Files;
            HttpPostedFile bgImg = UploadedFile[0];
            HttpPostedFile enVid= UploadedFile[1];
            HttpPostedFile frVid = UploadedFile[2];
            if (bgImg.ContentLength > 0 || enVid.ContentLength > 0 || frVid.ContentLength > 0 || topic != null)
            {
                topicService.AddTopic(bgImg, enVid, frVid, topic);
            }

            return View("Config");
        }


        public ActionResult Logout()
        {
            IAuthService authService = registry.AuthService;
            authService.Logout(Session);
            return View("Index");
        }

        public ActionResult Upload()
        {
            IAuthService authService = registry.AuthService;
            return View(authService.VerifyLogin("Upload", Session));
        }

        [ActionName("Login")]
        [AcceptVerbs("Post")]
        public ActionResult Login(FormCollection form)
        {
            IAuthService authService = registry.AuthService;
            if (authService.Login(form["user"], form["password"], Session))
            {
                return View("Upload");
            }

            return View("Index");
        }

        public ActionResult Metrics()
        {
            IAuthService authService = registry.AuthService;
            INarrativeService narrativeService = registry.NarrativeService;
            //ViewData["Dictionary"] = GetNonAnalyticMetrics();
            ViewData["views"] = narrativeService.GetTotalViews();
            ViewData["comments"] = narrativeService.GetTotalComments();
            ViewData["flagged"] = narrativeService.GetTotalFlagged();
            return View(authService.VerifyLogin("Metrics", Session));
        }

        public ActionResult List(string f, int start = 0, int end = 0)
        {
            IAuthService authService = registry.AuthService;
            INarrativeService narrativeService = registry.NarrativeService;
            if (authService.VerifyLogin("List", Session) != "List")
            {
                return View("Index");
            }

            if (f != null)
            {
                if (f == "flagged")
                    return View(narrativeService.GetNarratives(flagged: true));
                else if (f == "nc")
                    return View(narrativeService.GetNarratives(uncategorized: true));
            }

            return View(narrativeService.GetNarratives(start, end));
        }

        public ActionResult GetFlags(int id)
        {
            IAuthService authService = registry.AuthService;
            INarrativeService narrativeService = registry.NarrativeService;
            if (authService.VerifyLogin("List", Session) != "List")
            {
                return View("Index");
            }

            return Json(narrativeService.GetFlags(id), JsonRequestBehavior.AllowGet);
        }

        public ActionResult SetCategory(FormCollection form)
        {
            INarrativeService narrativeService = registry.NarrativeService;

            try
            {
                narrative n = new narrative();
                n.narrativeID = Int32.Parse(form["id"]);
                n.categoryID = Int32.Parse(form["form"]);

                List<Expression<Func<narrative, object>>> modified = new List<Expression<Func<narrative, object>>>();
                modified.Add(narr => narr.categoryID);

                narrativeService.Save(n, modified);
            }
            catch (Exception) {}

            return View("List", narrativeService.GetNarratives());
        }

        public ActionResult SetViewable(FormCollection form)
        {
            INarrativeService narrativeService = registry.NarrativeService;

            try
            {
                narrative n = new narrative();
                n.narrativeID = Int32.Parse(form["id"]);

                if (form["form"] == null)
                    n.userViewable = false;
                else
                    n.userViewable = true;

                List<Expression<Func<narrative, object>>> modified = new List<Expression<Func<narrative, object>>>();
                modified.Add(narr => narr.userViewable);

                narrativeService.Save(n, modified);
            }
            catch (Exception) {}

            return View("List", narrativeService.GetNarratives());
        }

        [HttpGet]
        public ActionResult ChangeTopic(int id)
        {
            ITopicService topicService = registry.TopicService;
            topicService.ChangeOnlineTopic(id);
            return View("Config");
        }

        [HttpGet]
        public ActionResult GetTopicList()
        {
            ITopicService topicService = registry.TopicService;
            List<string> topList = topicService.GetListAllTopics();
            return Json(topList, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult GetAdminList()
        {
            IAdminService adminService = registry.AdminService;
            List<string> adminList = adminService.GetAllAdminsString();
            return Json(adminList, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult Upload(HttpPostedFileBase file, FormCollection form)
        {
            IFileService fileService = registry.FileService;
            IUploadService uploadService = registry.UploadService;
            var directoryPath = new DirectoryInfoWrap(Server.MapPath("~/Uploads/Narratives"));
            var narPath = new DirectoryInfoWrap(directoryPath + @"\new\" + file.FileName);
            string path = null;
            string error = null;

            try
            {
                // extract zip file
                path = fileService.ExtractFile(file, directoryPath, narPath);

                // for each narrative in the folder
                string[] narrFiles = Directory.GetDirectories(narPath + @"\", "*.*");
                if (narrFiles == null || narrFiles.Length == 0)
                {
                    throw new ArgumentException("Zip Folder is Empty !");
                }
                uploadService.UploadNarratives(directoryPath, narPath, narrFiles);
                return RedirectToAction("List");
            }
            catch (Exception e)
            {
                error = e.Message;
            }
            finally
            {
                fileService.Delete(path);
                fileService.Delete(narPath.ToString());
            }
            return new HttpStatusCodeResult(550, error);
        }

        public ActionResult WriteToCsv()
        {
            var csv = new System.Text.StringBuilder();
            using (StreamWriter sw = new StreamWriter(Environment.GetFolderPath(Environment.SpecialFolder.Desktop) + "/test.csv", false))
            {
                var newline = "NARRATIVE ID,AGREES,DISAGREES,AMBIVALENT" + Environment.NewLine;
                csv.Append(newline);

                foreach (var item in GetNonAnalyticMetrics())
                {
                    Dictionary<string, int> metrics = item.Value;
                    newline = item.Key + "," + metrics["AGREES"] + "," + metrics["DISAGREES"] +
                              "," + metrics["AMBIVALENT"] + Environment.NewLine;
                    csv.Append(newline);
                }
                sw.Write(csv.ToString());
                ViewBag.Done = "CSV saved on your desktop with the name test.csv ";
                sw.Close();
            }

            return View("Metrics");
        }

        /// <summary>
        /// Returns a dictionary that maps narrative ID to 
        /// </summary>
        /// <returns></returns>
        public Dictionary<int, Dictionary<string, int>> GetNonAnalyticMetrics()
        {
            INarrativeService narrativeService = registry.NarrativeService;
            Dictionary<int, Dictionary<string, int>> metrics = new Dictionary<int, Dictionary<string, int>>();
            foreach (narrative narrative in narrativeService.GetNarratives())
            {
                Dictionary<string, int> metricValues = new Dictionary<string, int>();
                metricValues.Add("AGREES", GetNumberOfAgrees(narrative.narrativeID));
                metricValues.Add("DISAGREES", GetNumbersOfDisagrees(narrative.narrativeID));
                metricValues.Add("AMBIVALENT", GetNumberOfAmbivilent(narrative.narrativeID));

                metrics.Add(narrative.narrativeID, metricValues);
            }
            return metrics;
        }

        public int GetNumberOfAgrees(int id)
        {
            INarrativeService narrativeService = registry.NarrativeService;
            narrative n = narrativeService.GetNarrative(id);

            if (n != null)
            {
                return n.numberAgrees;
            }
            else
            {
                throw new NullReferenceException();
            }
        }

        public int GetNumbersOfDisagrees(int id)
        {
            INarrativeService narrativeService = registry.NarrativeService;
            narrative n = narrativeService.GetNarrative(id);

            if (n != null)
            {
                return n.numberDisagrees;
            }
            else
            {
                throw new NullReferenceException();
            }
        }

        public int GetNumberOfAmbivilent(int id)
        {
            INarrativeService narrativeService = registry.NarrativeService;
            narrative n = narrativeService.GetNarrative(id);

            if (n != null)
            {
                return n.numberAmbivalent;
            }
            else
            {
                throw new NullReferenceException();
            }
        }
    }
}
