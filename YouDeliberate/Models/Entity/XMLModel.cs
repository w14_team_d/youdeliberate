﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Xml.Serialization;
using YouDeliberate.Models.DAO;

namespace YouDeliberate.Models.Entity
{
    [XmlRoot("narrative")]
    public class XMLModel
    {
        private static string DATE_FORMAT = "yyyy-MM-dd HH-mm-ss";
        private static string CATEGORY = "None";

        [XmlIgnore]
        public INarrativeDAO narDAO { get; set; }
        [XmlIgnore]
        public ILanguageDAO langDAO { get; set; }
        [XmlIgnore]
        public ICategoryDAO categDAO { get; set; }

        public XMLModel()
        {
            narDAO = new NarrativeDAO();
            langDAO = new LanguageDAO();
            categDAO = new CategoryDAO();
        }

        public string narrativeName { get; set; }

        public string language { get; set; }

        public string submitDate { get; set; }

        public string time { get; set; }

        

        public narrative XMLtoDB(string path)
        {
            category category = new category();
            narrative narrative = new narrative();
            language language = new language();

            category = categDAO.retrieveCategoryByName(CATEGORY);
            language = langDAO.retrieveLanguageByName(this.language);

            string dateTime = this.submitDate + " " + this.time;

            DateTime dateNar = DateTime.ParseExact(dateTime, DATE_FORMAT,
                                        System.Globalization.CultureInfo.InvariantCulture);

            narrative.userViewable = true;
            narrative.path = path;
            narrative.flagged = false;
            narrative.languageID = language.languageID;
            narrative.categoryID = category.categoryID;
            narrative.date = dateNar;
            narrative.name = this.narrativeName;
            narrative.numberAgrees = 0;
            narrative.numberDisagrees = 0;
            narrative.numberViews = 0;
            narrative.numberAmbivalent = 0;

            narDAO.InsertNarrative(narrative);

            return narrative;
        }

        public static XMLModel ParseXML(string xml)
        {
            XMLModel narrative = new XMLModel();
            using (TextReader stream = new StringReader(xml))
            {
                XmlSerializer xmlSerializer = new XmlSerializer(typeof(XMLModel));
                narrative = (XMLModel)xmlSerializer.Deserialize(stream);
            }
            return narrative;
        }
    }
}