﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using YouDeliberate.Models.Entity;

namespace YouDeliberate.Models.Services
{
    public interface IAdminService
    {
        int delId { get; set; }

        /// <summary>
        /// Select Config Admin Options - add, mod, del
        /// </summary>
        /// <param name="option"></param>
        void selectOption(administrator admin, string option);

        /// <summary>
        /// Retrieve List of Admins
        /// </summary>
        /// <returns>List of Administrators</returns>
        List<administrator> GetAllAdmins();

        /// <summary>
        /// Retrieve List of Admins String
        /// </summary>
        /// <returns>String List of Administrators</returns>
        List<string> GetAllAdminsString();

        /// <summary>
        /// Add a New Administarot
        /// </summary>
        /// <param name="userName">Administrator Username</param>
        /// <param name="password">Administrator Password</param>
        /// <param name="email">Administrator Email</param>
        /// <returns></returns>
        administrator AddAdministrator(string userName, String password, String email);

        /// <summary>
        /// Modify Administrator
        /// </summary>
        /// <param name="username">Administrator Username</param>
        /// <param name="password">Administrator Password</param>
        /// <param name="email">Administator Email</param>
        void ModifyAdministrator(string username, String password, String email);

        /// <summary>
        /// Delete Administrator
        /// </summary>
        /// <param name="id">Administrator Id</param>
        void DeleteAdministrator(int id);
    }
}
