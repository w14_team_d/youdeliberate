﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using YouDeliberate.Models.DAO;
using YouDeliberate.Models.Entity;

namespace YouDeliberate.Models.Services
{
    public class AdminService : IAdminService
    {
        private IAdminDAO adminDAO;

        public AdminService()
        {
            adminDAO = new AdminDAO();
        }

        public AdminService(IAdminDAO iadao)
        {
            adminDAO = iadao;
        }

        private Dictionary<String, Action> actions;

        private string adminUser;

        private string adminPass;

        private string adminEmail;
        public int delId { get; set; }

        /// <summary>
        /// Set Options
        /// </summary>
        private void setOptions()
        {
            AdminService adminService = new AdminService();
            actions = new Dictionary<String, Action>(){

                        {"add", () => {adminService.AddAdministrator(adminUser, adminPass, adminEmail);} }, 
                        {"mod", () => { adminService.ModifyAdministrator(adminUser, adminPass, adminEmail);} },
                        {"del", () => { adminService.DeleteAdministrator(delId); } }
            };
        }

        /// <summary>
        /// Select Options
        /// </summary>
        /// <param name="option"></param>
        public void selectOption(administrator admin,string option)
        {
            if(admin != null)
            {
                adminUser = admin.username;
                adminPass = admin.password;
                adminEmail = admin.email;
            }
            setOptions();
            actions[option]();
        }

        public List<administrator> GetAllAdmins()
        {
            return adminDAO.GetAdmins();
        }

        public List<string> GetAllAdminsString()
        {
            List<administrator> adminList = new List<administrator>();
            adminList = adminDAO.GetAdmins();
            List<string> result = new List<string>();
            foreach(administrator admin in adminList)
            {
                result.Add(admin.adminID.ToString());
                result.Add(admin.username);
                result.Add(admin.email);
            }
            return result;
        }

        /// <summary>
        /// Add Administrator
        /// </summary>
        /// <param name="userName"></param>
        /// <param name="password"></param>
        /// <param name="email"></param>
        /// <returns></returns>
        public administrator AddAdministrator(string userName, String password, String email)
        {
            administrator admin = new administrator();
            EncryptService encrypt = new EncryptService();

            admin.username = userName;
            admin.password = encrypt.Encrypt(password);
            admin.email = email;

            return adminDAO.InsertAdmin(admin);
        }

        /// <summary>
        /// Modify Adminsitrator
        /// </summary>
        /// <param name="username"></param>
        /// <param name="password"></param>
        /// <param name="email"></param>
        public void ModifyAdministrator(string username, String password, String email)
        {
            administrator admin = new administrator();
            EncryptService encrypt = new EncryptService();
            string passEncr = encrypt.Encrypt(password);

            adminDAO.UpdateAdministrator(username, passEncr, email);
        }

        /// <summary>
        /// Delete Administrator
        /// </summary>
        /// <param name="id"></param>
        public void DeleteAdministrator(int id)
        {
            adminDAO.DeleteAdministrator(id);
        }


    }
}