﻿using System;
using System.Linq.Expressions;
using YouDeliberate.Models.Entity;

namespace YouDeliberate.Models.Filters
{
    public interface IFilter
    {
        Expression<Func<narrative, bool>> GetFilter();
    }
}