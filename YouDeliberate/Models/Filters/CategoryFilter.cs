﻿using System;
using System.Linq.Expressions;
using YouDeliberate.Models.Entity;

namespace YouDeliberate.Models.Filters
{
    public class CategoryFilter : IFilter
    {
        private string Category { get; set; }
        private int CategoryID { get; set; }
        private bool IsCategory { get; set; }
        public CategoryFilter(string s, bool b)
        {
            Category = s;
            IsCategory = b;
        }

        public CategoryFilter(int id, bool b)
        {
            CategoryID = id;
            IsCategory = b;
        }

        public Expression<Func<narrative, bool>> GetFilter()
        {
            Expression<Func<narrative, bool>> expr = null;
            if(Category != null)
            {
                if (IsCategory)
                {
                    expr = n => n.category.category1.Equals(Category);
                }
                else
                {
                    expr = n => !n.category.category1.Equals(Category);
                } 
            }
            else if (CategoryID > 0)
            {
                if (IsCategory)
                {
                    expr = n => n.category.categoryID.Equals(CategoryID);
                }
                else
                {
                    expr = n => !n.category.categoryID.Equals(CategoryID);
                }
            }
            return expr;
        }
    }
}