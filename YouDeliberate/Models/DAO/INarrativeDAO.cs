﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity.Infrastructure;
using System.Data.Objects;
using System.Linq;
using System.Linq.Expressions;
using YouDeliberate.Models.Entity;
using YouDeliberate.Models.Filters;

namespace YouDeliberate.Models.DAO
{
    public interface INarrativeDAO
    {
        narrative InsertNarrative(narrative viewpoints);
        narrative RetrieveNarrative(int id);
        List<narrative> RetrieveAllNarratives();
        narrative UpdateNarrative(narrative n, List<Expression<Func<narrative, object>>> modified);
        ICollection<narrative> GetFilteredList(int a, int b, IEnumerable<IFilter> filters);
        comment AddComment(comment c);
        void AddComOnCom(comment com, int parent);
        bool AddFlag(narrativeflag f);
        void UpdateAudio(narrative n);
        void UpdatePicture(narrative n);
    }
}