﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using YouDeliberate.Models.Entity;

namespace YouDeliberate.Models.DAO
{
    public interface ITopicDAO
    {
        /// <summary>
        /// Insert a new Topic
        /// </summary>
        /// <param name="viewpoints">Topic Object</param>
        /// <returns>The new Topic Inserted</returns>
        topic InsertTopic(topic viewpoints);

        /// <summary>
        /// Update a Topic
        /// </summary>
        /// <param name="tp">Topic Object</param>
        /// <param name="modified">Topic Oject Modified</param>
        /// <returns>The new Updated Topic</returns>
        topic UpdateTopic(topic tp, List<Expression<Func<topic, object>>> modified);

        /// <summary>
        /// Delete a Topic by Id
        /// </summary>
        /// <param name="id">Topic To be Deleted</param>
        void DeleteTopic(int id);

        /// <summary>
        /// Get the Topic List
        /// </summary>
        /// <returns>List of Topics</returns>
        List<topic> GetTopicList();

        /// <summary>
        /// Get a Specific Topic by Id
        /// </summary>
        /// <param name="id">The required Topic</param>
        /// <returns>Topic Object With the Topic Required</returns>
        topic GetTopic(int id);

        /// <summary>
        /// Get the Current Topic
        /// </summary>
        /// <returns>The Topic Object that is currently in use</returns>
        topic GetCurTopic();
    }
}
