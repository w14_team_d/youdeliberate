﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using YouDeliberate.Models.Entity;

namespace YouDeliberate.Models.DAO
{
    public class CommentDAO : ICommentDAO
    {
        public List<comment> GetComments(int nid)
        {
            List<comment> comments = new List<comment>();

            using (var db = new soen390w14teamdEntities())
            {
                try
                {
                    narrative n = db.narratives.First(narr => narr.narrativeID == nid);

                    comments =
                    db.comments.SqlQuery("SELECT * FROM comment WHERE " +
                                         "narrativeID = @p0 AND " +
                                         "commentID NOT IN (" +
                                            "SELECT commentID FROM commentchildof" +
                                         ")", nid).ToList();
                    LoadHierarchy(comments);
                }
                catch (Exception)
                {
                    comments = null;
                }
            }
            return comments;
        }

        public int GetNrComments()
        {
            var count = 0;

            using (var db = new soen390w14teamdEntities())
            {
                try
                {
                    narrative n = db.narratives.First();

                    count =
                    db.comments.SqlQuery("SELECT * FROM comment").Count();
                }
                catch (Exception)
                {
                    count = -1;
                }
            }
            return count;
        }

        public List<comment> GetComments(int start,int end)
        {
            List<comment> comments = new List<comment>();
            int quantity = end - start;

            using (var db = new soen390w14teamdEntities())
            {
                try
                {
                    narrative n = db.narratives.First();

                    comments =
                    db.comments.SqlQuery("SELECT * FROM comment").Skip(start).Take(quantity).ToList();
                    LoadHierarchy(comments);
                }
                catch (Exception)
                {
                    comments = null;
                }
            }
            return comments;
        }

        public List<comment> GetChildComments(int nid,int cid)
        {
            List<comment> comments = new List<comment>();
            using (var db = new soen390w14teamdEntities())
            {

                try
                {
                    comments =
                    db.comments.SqlQuery("SELECT * FROM comment WHERE " +
                                         "narrativeID = @p0 AND " +
                                         "commentID IN (" +
                                            "SELECT commentID FROM commentchildof WHERE parentID = @p1" +
                                         ")", nid, cid).ToList();
                    LoadHierarchy(comments);
                }
                catch(Exception)
                {
                    comments = null;
                }
            }
            return comments;
        }
        public bool DeleteComment(int id)
        {
            using (var db = new soen390w14teamdEntities())
            {
                try
                {
                    var sql = @"DELETE FROM comment WHERE commentID = {0}";
                    db.Database.ExecuteSqlCommand(sql, id);
                    return true;
                }
                catch (Exception)
                {
                    return false;
                }
            }
        }
        private void LoadHierarchy(ICollection<comment> commentsToLoad)
        {
            foreach(comment c in commentsToLoad)
            {
                if(c.comments.Count > 0)
                {
                    this.LoadHierarchy(c.comments);
                }
            }
        }
    }
}