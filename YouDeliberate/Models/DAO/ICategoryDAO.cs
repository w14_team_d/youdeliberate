﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace YouDeliberate.Models.DAO
{
    public interface ICategoryDAO
    {
        /// <summary>
        /// Retrieve Category By Name
        /// </summary>
        /// <param name="CATEGORY">Category</param>
        /// <returns>Category Object</returns>
        Entity.category retrieveCategoryByName(string CATEGORY);
    }
}
