﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using YouDeliberate.Models.Entity;

namespace YouDeliberate.Models.DAO
{
    public interface IAdminDAO
    {
        /// <summary>
        /// Insert a new Administrator
        /// </summary>
        /// <param name="viewpoints">The Admin Object to be inserted</param>
        /// <returns>Admin Object inserted into the database</returns>
        administrator InsertAdmin(administrator viewpoints);

        /// <summary>
        /// Delete the Administrator
        /// </summary>
        /// <param name="id">Administrator Id to be deleted</param>
        void DeleteAdministrator(int id);

        /// <summary>
        /// Retrieve Administrator
        /// </summary>
        /// <param name="username">Administrator Username</param>
        /// <param name="password">Administrator Password</param>
        /// <returns>Administrator Object</returns>
        administrator RetrieveAdmin(String username, String password);

        /// <summary>
        /// Retreive Administrator
        /// </summary>
        /// <param name="username">Administrator Username</param>
        /// <returns>The Administrator Id</returns>
        int GetAdminId(String username);

        /// <summary>
        /// Update Administrator
        /// </summary>
        /// <param name="username">Administrator Username</param>
        /// <param name="password">Adminsitator Password</param>
        /// <param name="email">Administator Email</param>
        void UpdateAdministrator(String username, String password, String email);
        
        /// <summary>
        /// Get list of All Administrators
        /// </summary>
        /// <returns>List of Administators</returns>
        List<administrator> GetAdmins();

    }
}