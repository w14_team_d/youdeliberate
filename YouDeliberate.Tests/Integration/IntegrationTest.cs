﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Web.Mvc;
using MvcIntegrationTestFramework.Hosting;
using MvcIntegrationTestFramework.Browsing;

namespace YouDeliberate.Tests.Integration
{
    [TestClass]
    public class IntegrationTest
    {
        private readonly AppHost _appHost = AppHost.Simulate("YouDeliberate");

        [TestMethod]
        public void Player()
        {
            _appHost.Start(browsingSession =>
            {
                RequestResult result = browsingSession.Get("Browse/Index");

                // Routing config should match "Browse" controller
                Assert.AreEqual("Browse", result.ActionExecutedContext.RouteData.Values["Controller"]);

                // Should have rendered the "index" view
                ActionResult actionResult = result.ActionExecutedContext.Result;
                Assert.IsInstanceOfType(actionResult, typeof(ViewResult));
                Assert.AreEqual("Index", ((ViewResult)actionResult).ViewName);

                // App should not have had an unhandled exception
                Assert.IsNull(result.ResultExecutedContext.Exception);
            });
        }

        [TestMethod]
        public void Sorting()
        {
            _appHost.Start(browsingSession =>
            {
                RequestResult result = browsingSession.Get("Browse/Index");

                // Routing config should match "" controller
                Assert.AreEqual("Browse", result.ActionExecutedContext.RouteData.Values["Controller"]);

                // Should have rendered the "" view
                ActionResult actionResult = result.ActionExecutedContext.Result;
                Assert.IsInstanceOfType(actionResult, typeof(ViewResult));
                Assert.AreEqual("Index", ((ViewResult)actionResult).ViewName);

                // App should not have had an unhandled exception
                Assert.IsNull(result.ResultExecutedContext.Exception);
            });
        }

        [TestMethod]
        public void Filtering()
        {
            
           _appHost.Start(browsingSession =>
            {
                RequestResult result = browsingSession.Get("Browse/Index");

                // Routing config should match "" controller
                Assert.AreEqual("Browse", result.ActionExecutedContext.RouteData.Values["Controller"]);

                // Should have rendered the "" view
                ActionResult actionResult = result.ActionExecutedContext.Result;
                Assert.IsInstanceOfType(actionResult, typeof(ViewResult));
                Assert.AreEqual("Index", ((ViewResult)actionResult).ViewName);

                // App should not have had an unhandled exception
                Assert.IsNull(result.ResultExecutedContext.Exception);
            });
        }
    }
}
