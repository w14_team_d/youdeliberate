﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using MvcIntegrationTestFramework.Browsing;
using MvcIntegrationTestFramework.Hosting;
using System.Collections.Generic;
using System.Web.Mvc;
using System.Linq;
using System.Linq.Expressions;
using YouDeliberate.Controllers;
using YouDeliberate.Models.Entity;
using YouDeliberate.Models.Services;
using System;
using YouDeliberate.Models.DAO;
using System.Net.Http;
using System.Web;
using System.Collections.Specialized;

namespace YouDeliberate.Tests.Controllers
{
    [TestClass]
    public class NarrativeControllerTest
    {
        [TestMethod]
        public void LoadPlayerBadID()
        {
            Mock<INarrativeService> dataService = new Mock<INarrativeService>();
            dataService.Setup(ds => ds.GetNarrative(-1)).Returns(() => null);

            NarrativeController controller = new NarrativeController(dataService.Object);
            HttpStatusCodeResult result = controller.LoadPlayer(-1) as HttpStatusCodeResult;

            Assert.AreEqual(550, result.StatusCode);
        }

        [TestMethod]
        public void LoadPlayer()
        {
            narrative mock = new narrative();

            Mock<INarrativeService> dataService = new Mock<INarrativeService>();
            dataService.Setup(ds => ds.GetNarrative(It.Is<int>(i => i == 1))).Returns(() => mock);

            NarrativeController controller = new NarrativeController(dataService.Object);
            JsonResult jsonResult = controller.LoadPlayer(1) as JsonResult;

            Dictionary<string, object> result = jsonResult.Data as Dictionary<string, object>;

            Assert.IsTrue(result.ContainsKey("comments"));
            Assert.IsTrue(result.ContainsKey("agrees"));
            Assert.IsTrue(result.ContainsKey("disagrees"));
            Assert.IsTrue(result.ContainsKey("ambivalent"));
            Assert.IsTrue(result.ContainsKey("views"));
        }

        [TestMethod]
        public void VoteBadID()
        {
            Mock<INarrativeService> dataService = new Mock<INarrativeService>();
            dataService.Setup(ds => ds.GetNarrative(1)).Returns(() => null);

            NarrativeController controller = new NarrativeController(dataService.Object);
            HttpStatusCodeResult result = controller.Vote("NONE", 1) as HttpStatusCodeResult;

            Assert.AreEqual(500, result.StatusCode);
        }

        [TestMethod]
        public void Vote()
        {
            Mock<INarrativeService> dataService = new Mock<INarrativeService>();
            dataService.Setup(ds => ds.GetNarrative(1)).Returns(() => new narrative());
            dataService.Setup(ds => ds.Save(It.IsAny<narrative>(), It.IsAny<List<Expression<Func<narrative, object>>>>()));

            NarrativeController controller = new NarrativeController(dataService.Object);
            HttpStatusCodeResult result = controller.Vote("Agree", 1) as HttpStatusCodeResult;

            Assert.AreEqual(200, result.StatusCode);
            dataService.Verify(ds => ds.Save(
                It.IsAny<narrative>(),
                It.Is<List<Expression<Func<narrative, object>>>>(l =>
                    l.Contains((n => n.numberAgrees), new LambdaComparison())
                )
            )); // Verify that the number of agrees was updated
        }

        [TestMethod]
        public void ViewBadID()
        {
            Mock<INarrativeService> dataService = new Mock<INarrativeService>();
            dataService.Setup(ds => ds.GetNarrative(-1)).Returns(() => null);

            NarrativeController controller = new NarrativeController(dataService.Object);
            HttpStatusCodeResult result = controller.View(-1) as HttpStatusCodeResult;

            Assert.AreEqual(500, result.StatusCode);
        }

        [TestMethod]
        public void View()
        {
            Mock<INarrativeService> dataService = new Mock<INarrativeService>();
            dataService.Setup(ds => ds.GetNarrative(1)).Returns(() => new narrative());

            NarrativeController controller = new NarrativeController(dataService.Object);
            HttpStatusCodeResult result = controller.View(1) as HttpStatusCodeResult;

            Assert.AreEqual(200, result.StatusCode);
        }

        [TestMethod]
        public void TestAddComment()
        {
            Mock<INarrativeDAO> indao = new Mock<INarrativeDAO>();

            NarrativeService ds = new NarrativeService(indao.Object);
            NarrativeController controller = new NarrativeController(ds);

            HttpRequestMessage message = new HttpRequestMessage();
            Mock<HttpContextBase> context = new Mock<HttpContextBase>();
            Mock<HttpRequestBase> requestBase = new Mock<HttpRequestBase>();

            NameValueCollection httpValues = new NameValueCollection();
            httpValues.Add("HTTP_X_FORWARDED_FOR", "127.0.0.1");
            httpValues.Add("HTTP_VIA", "");

            message.Properties["MS_HttpContext"] = context.Object;

            context.Setup(c => c.Request).Returns(requestBase.Object);
            requestBase.Setup(rb => rb.ServerVariables).Returns(httpValues);

            string hash = controller.AddComment(-1, "TEST", message);
            Assert.IsTrue(hash.Length == 32);
        }

        [TestMethod]
        public void TestAddComOnComment()
        {
            Mock<INarrativeDAO> indao = new Mock<INarrativeDAO>();

            NarrativeService ds = new NarrativeService(indao.Object);
            NarrativeController controller = new NarrativeController(ds);

            HttpRequestMessage message = new HttpRequestMessage();
            Mock<HttpContextBase> context = new Mock<HttpContextBase>();
            Mock<HttpRequestBase> requestBase = new Mock<HttpRequestBase>();

            NameValueCollection httpValues = new NameValueCollection();
            httpValues.Add("HTTP_X_FORWARDED_FOR", "127.0.0.1");
            httpValues.Add("HTTP_VIA", "");

            message.Properties["MS_HttpContext"] = context.Object;

            context.Setup(c => c.Request).Returns(requestBase.Object);
            requestBase.Setup(rb => rb.ServerVariables).Returns(httpValues);

            ActionResult hash = controller.AddComOnComment(-1,-1, "TEST", message);
            Assert.IsFalse(hash == null);
        }

        [TestMethod]
        public void TestAddCommentNoHttpVia()
        {
            Mock<INarrativeDAO> indao = new Mock<INarrativeDAO>();

            NarrativeService ds = new NarrativeService(indao.Object);
            NarrativeController controller = new NarrativeController(ds);

            HttpRequestMessage message = new HttpRequestMessage();
            Mock<HttpContextBase> context = new Mock<HttpContextBase>();
            Mock<HttpRequestBase> requestBase = new Mock<HttpRequestBase>();

            NameValueCollection httpValues = new NameValueCollection();
            httpValues.Add("REMOTE_ADDR", "127.0.0.1");
            httpValues.Add("HTTP_VIA", null);

            message.Properties["MS_HttpContext"] = context.Object;

            context.Setup(c => c.Request).Returns(requestBase.Object);
            requestBase.Setup(rb => rb.ServerVariables).Returns(httpValues);

            string hash = controller.AddComment(-1, "TEST", message);
            Assert.IsTrue(hash.Length == 32);
        }

        [TestMethod]
        public void TestGetComments()
        {
            Mock<INarrativeDAO> indao = new Mock<INarrativeDAO>();
            indao.Setup(i => i.AddComment(new comment()));

            NarrativeService ns = new NarrativeService(indao.Object);
            NarrativeController controller = new NarrativeController(ns);

            JsonResult jsonResult = controller.GetComments(1) as JsonResult;
            List<string> result = jsonResult.Data as List<string>;
            Assert.IsNull(result);
        }

        [TestMethod]
        public void TestAddFlagNoComment()
        {
            Mock<INarrativeDAO> indao = new Mock<INarrativeDAO>();
            indao.Setup(i => i.AddFlag(It.IsAny<narrativeflag>())).Returns(true);

            NarrativeService ds = new NarrativeService(indao.Object);
            NarrativeController controller = new NarrativeController(ds);

            Assert.IsTrue(controller.AddFlag(-1, null));
        }

        [TestMethod]
        public void TestAddFlag()
        {
            Mock<INarrativeDAO> indao = new Mock<INarrativeDAO>();
            indao.Setup(i => i.AddFlag(It.IsAny<narrativeflag>())).Returns(true);

            NarrativeService ds = new NarrativeService(indao.Object);

            Assert.IsTrue(ds.AddFlag(1, "this narrative is not nice"));
        }

        private class LambdaComparison : IEqualityComparer<Expression<Func<narrative, object>>>
        {
            public bool Equals(Expression<Func<narrative, object>> e1, Expression<Func<narrative, object>> e2)
            {
                return e1.ToString().Equals(e2.ToString());
            }

            public int GetHashCode(Expression<Func<narrative, object>> e)
            {
                return e.GetHashCode();
            }
        }
    }
}
