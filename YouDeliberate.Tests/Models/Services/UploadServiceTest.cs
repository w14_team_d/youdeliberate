﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Linq;
using SystemWrapper.IO;
using YouDeliberate.Models.Entity;
using YouDeliberate.Models.Services;

namespace YouDeliberate.Tests.Controllers
{
    [TestClass]
    public class UploadServiceTest
    {
        private UploadService upload = new UploadService(new Mock<INarrativeService>().Object);

        [TestMethod]
        public void TestGetFilesFromDirectory()
        {
            string[] extensions = { "*.mp3", "*.wav" };
            Mock<IDirectoryInfoWrap> path = new Mock<IDirectoryInfoWrap>();
            path.Setup(p => p.GetFiles("*.mp3")).Returns(new IFileInfoWrap[] {new FileInfoWrap("1.mp3")});

            string[] result = upload.GetFilesFromDirectory(path.Object, extensions);
            Assert.AreEqual(1, result.Length);

            path.Verify(p => p.GetFiles("*.mp3"));
            path.Verify(p => p.GetFiles("*.wav"));
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void TestValidateXMLLanguageInput()
        {
            DirectoryInfoWrap direct = new DirectoryInfoWrap("c:/");

            XMLModel model = new XMLModel();
            model.narrativeName = "dada";
            model.language = "bla bla bla";
            model.submitDate = "2013-01-12";
            model.time = "00-00-00";

            upload.ValidateXMLInput(model, direct);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void TestValidateXMLDateInput()
        {
            DirectoryInfoWrap direct = new DirectoryInfoWrap("c:/");

            XMLModel model = new XMLModel();
            model.narrativeName = "dada";
            model.language = "English";
            model.submitDate = "2013-01-99";
            model.time = "00-00-00";

            upload.ValidateXMLInput(model, direct);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void TestValidateXMLTimeInput()
        {
            DirectoryInfoWrap direct = new DirectoryInfoWrap("c:/");

            XMLModel model = new XMLModel();
            model.narrativeName = "dada";
            model.language = "English";
            model.submitDate = "2013-01-21";
            model.time = "none";

            upload.ValidateXMLInput(model, direct);
        }

        [TestMethod]
        public void TestValidateXMLInput()
        {
            DirectoryInfoWrap direct = new DirectoryInfoWrap("c:/");

            XMLModel model = new XMLModel();
            model.narrativeName = "dada";
            model.language = "English";
            model.submitDate = "2013-01-21";
            model.time = "10-10-10";

            upload.ValidateXMLInput(model, direct);
        }

        [TestMethod]
        public void TestAddAudioFiles()
        {
            Mock<IDirectoryInfoWrap> narrativePath = new Mock<IDirectoryInfoWrap>();
            string fakeAudio = "C:\\FAKE\\PATH\\TEST\\1.mp3";
            narrativePath.Setup(p => p.GetFiles("*.mp3"))
                .Returns(new IFileInfoWrap[] { new FileInfoWrap(fakeAudio) });

            narrative n = new narrative();
            upload.AddAudioFiles(n, narrativePath.Object);

            Assert.IsTrue(n.audios.All(a => a.path == "1.mp3"));
        }

        [TestMethod]
        public void TestAddPictureFiles()
        {
            Mock<IDirectoryInfoWrap> narrativePath = new Mock<IDirectoryInfoWrap>();
            string fakeAudio = "C:\\FAKE\\PATH\\TEST\\1.jpg";
            narrativePath.Setup(p => p.GetFiles("*.jpg"))
                .Returns(new IFileInfoWrap[] { new FileInfoWrap(fakeAudio) });

            narrative n = new narrative();
            upload.AddPictureFiles(n, narrativePath.Object);

            Assert.IsTrue(n.pictures.All(a => a.path == "1.jpg"));
        }
    }
}
