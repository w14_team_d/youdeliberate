﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using YouDeliberate.Models.DAO;
using YouDeliberate.Models.Entity;
using YouDeliberate.Models.Filters;
using YouDeliberate.Models.Services;

namespace YouDeliberate.Tests.Models.Services
{
    [TestClass]
    public class DataServiceTest
    {

        [TestMethod]
        public void TestAddComment()
        {
            Mock<INarrativeDAO> indao = new Mock<INarrativeDAO>();
            indao.Setup(i => i.AddComment(It.IsAny<comment>()));

            NarrativeService ds = new NarrativeService(indao.Object);

            Assert.IsTrue(ds.AddComment(1, "test comment", "192.168.1.0").Length == 32);
        }

        [TestMethod]
        public void TestAddFlag()
        {
            Mock<INarrativeDAO> indao = new Mock<INarrativeDAO>();
            indao.Setup(i => i.AddFlag(It.IsAny<narrativeflag>())).Returns(true);

            NarrativeService ds = new NarrativeService(indao.Object);

            Assert.IsTrue(ds.AddFlag(1, "this narrative is not nice"));
        }

        [TestMethod]
        public void TestGetAdmins()
        {
            Mock<IAdminDAO> iadao = new Mock<IAdminDAO>();
            iadao.Setup(i => i.GetAdmins()).Returns(() => new List<administrator>());

            //NarrativeService ds = new NarrativeService(iadao.Object, null);
            AdminService ds = new AdminService(iadao.Object);

            Assert.IsNotNull(ds.GetAllAdmins());
        }

        [TestMethod]
        public void TestGetComment()
        {
            Mock<INarrativeDAO> indao = new Mock<INarrativeDAO>();
            indao.Setup(i => i.RetrieveNarrative(It.IsAny<int>())).Returns(() => new narrative());

            NarrativeService ds = new NarrativeService(indao.Object);

            Assert.IsNotNull(ds.GetComments(1));
        }

        [TestMethod]
        public void TestGetFlags()
        {
            Mock<INarrativeDAO> indao = new Mock<INarrativeDAO>();
            indao.Setup(i => i.RetrieveNarrative(It.IsAny<int>())).Returns(() => new narrative());

            NarrativeService ds = new NarrativeService(indao.Object);

            Assert.IsNotNull(ds.GetFlags(1));
        }

        [TestMethod]
        public void TestGetNarrative()
        {
            Mock<INarrativeDAO> indao = new Mock<INarrativeDAO>();
            indao.Setup(i => i.RetrieveNarrative(It.IsAny<int>())).Returns(() => new narrative());

            NarrativeService ds = new NarrativeService(indao.Object);

            Assert.IsNotNull(ds.GetNarrative(1));
        }

        [TestMethod]
        public void TestGetNarratives()
        {
            Mock<INarrativeDAO> indao = new Mock<INarrativeDAO>();
            indao.Setup(i => i.GetFilteredList(It.IsAny<int>(), It.IsAny<int>(), It.IsAny<IEnumerable<IFilter>>())).Returns(() => new List<narrative>());

            NarrativeService ds = new NarrativeService(indao.Object);

            Assert.IsNotNull(ds.GetNarratives(from : 0, to : 0));
            Assert.IsNotNull(ds.GetNarratives(from: 0, to: 0, viewable : true));
            Assert.IsNotNull(ds.GetNarratives(from: 0, to: 0, flagged: true));
            Assert.IsNotNull(ds.GetNarratives(from: 0, to: 0, uncategorized: true));
        }

        [TestMethod]
        public void TestGetPlaylist()
        {
        }

        [TestMethod]
        public void TestSave()
        {
            narrative n = new narrative();


            Mock<INarrativeDAO> indao = new Mock<INarrativeDAO>();
            indao.Setup(i => i.InsertNarrative(It.IsAny<narrative>())).Returns(() => new narrative());
            indao.Setup(i => i.UpdateNarrative(It.IsAny<narrative>(), It.IsAny<List<Expression<Func<narrative, object>>>>())).Returns(() => new narrative());

            NarrativeService ds = new NarrativeService(indao.Object);

            //Assert.IsNull(null);

            n.narrativeID = -1;
            Assert.IsNotNull(ds.Save(n));
        }

        [TestMethod]
        public void TestSortNarratives()
        {
            narrative n1 = new narrative();
            n1.numberAgrees = 4;
            n1.date = new DateTime(5);

            narrative n2 = new narrative();
            n2.numberAgrees = 3;
            n2.date = new DateTime(1);

            narrative n3 = new narrative();
            n3.numberAgrees = 5;
            n3.date = new DateTime(0);

            List<narrative> narratives = new List<narrative>();
            narratives.Add(n2);
            narratives.Add(n3);
            narratives.Add(n1);

            List<narrative> expected = new List<narrative>();
            expected.Add(n1);
            expected.Add(n2);
            expected.Add(n3);

            // Get the newest narratives then use the number of agrees as a tiebreaker
            narratives = (new NarrativeService()).SortNarratives(narratives, "DateAdded");
            Assert.IsTrue(expected.SequenceEqual(narratives));
        }

        [TestMethod]
        public void TestAddChildCom()
        {
            Mock<INarrativeDAO> indao = new Mock<INarrativeDAO>();
            indao.Setup(i => i.AddComment(It.IsAny<comment>()));

            NarrativeService ds = new NarrativeService(indao.Object);
            ds.AddChildCom(1, 1, "test comment", "192.168.1.0");

            Assert.IsNotNull(ds.GetChildComments(1,1));
        }

        [TestMethod]
        public void TestGetChildComments()
        {
            Mock<INarrativeDAO> indao = new Mock<INarrativeDAO>();
            indao.Setup(i => i.RetrieveNarrative(It.IsAny<int>())).Returns(() => new narrative());

            NarrativeService ds = new NarrativeService(indao.Object);

            Assert.IsNotNull(ds.GetChildComments(1,1));
        }
    }
}
