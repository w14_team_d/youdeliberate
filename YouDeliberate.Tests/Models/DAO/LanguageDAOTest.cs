﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using YouDeliberate.Models.DAO;
using YouDeliberate.Models.Entity;

namespace YouDeliberate.Tests.Models
{
    [TestClass]
    public class LanguageDAOTest
    {
        [TestMethod]
        public void retrieve_Language_By_ID_Test()
        {
            LanguageDAO dao = new LanguageDAO();
            language expected = new language();
            expected.language1 = "English";
            language actual = dao.retrieveLanguageByID(1);
            Assert.AreEqual(expected.language1, actual.language1, true);
        }

        [TestMethod]
        public void retrieve_Language_By_Name_Test()
        {
            LanguageDAO dao = new LanguageDAO();
            language expected = new language();
            expected.languageID = 1;
            language actual = dao.retrieveLanguageByName("English");
            Assert.AreEqual<int>(expected.languageID, actual.languageID);
        }

        [TestMethod]
        public void retrieve_Non_Existant_Language_By_ID_Test()
        {
            LanguageDAO dao = new LanguageDAO();
            language actual = dao.retrieveLanguageByID(-1);
            Assert.IsNull(actual);
        }
    }
}
